import { Controller, Get, Body, Post, Res, Param, Put } from '@nestjs/common';
import { StudentService } from 'src/provider/student.service';
import { Student } from 'src/schema/student.schema';
import { BaseDTO } from 'src/dto/base.dto';
import { Observable } from 'rxjs';
import { Response } from 'express';
import { TableResponse } from 'src/dto/table-response.dto';
import { StudentDTO } from 'src/dto/student.dto';

@Controller('student')
export class StudentController {

  constructor(private studentService: StudentService) { }


  @Post('/find/all')
  findAll(@Body() baseDTO: BaseDTO): Promise<TableResponse<Student[]> | Student[]> {
    return this.studentService.findAll(baseDTO);
  }

  @Get('/find/:id')
  findById(@Param('id') id): Promise<Student> {
    return this.studentService.findById(id);
  }

  @Post('/add')
  save(@Body() student: Student): Promise<Student[]> {
    return this.studentService.save(student);
  }

  @Post('/update')
  update(@Body() student: Student): Promise<Student[]> {
    return this.studentService.update(student);
  }

  @Post('/search')
  search(@Body() base: BaseDTO): Promise<TableResponse<Student[]>> {
    return this.studentService.search(base);
  }

  @Get('/count')
  count(): Promise<number> {
    return this.studentService.count();
  }


  @Post('/voucher/downlaod/paid')
  async downloadVoucherPaid(@Res() res: Response, @Body() student: StudentDTO): Promise<void> {
    const buffer = await this.studentService.downlaodVoucher(student, res)
    res.end(buffer)
  }

  @Post('/voucher/downlaod/unpaid')
  async downloadVoucherUnpaid(@Res() res: Response, @Body() student: StudentDTO): Promise<void> {
    const buffer = await this.studentService.generateVoucher(student, res)
    res.end(buffer);
  }

  @Put('/voucher')
  updateVoucher(@Body() student: StudentDTO): Promise<any> {
    return this.studentService.updateVoucher(student);
  }


  @Get('/hello')
  hello(): String {
    return "Hello";
  }

}

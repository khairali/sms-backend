import { Controller, Get, Param, Query } from '@nestjs/common';
import { Observable } from 'rxjs';
import { DataService } from 'src/provider/data.service';
import {AxiosResponse } from 'axios';

@Controller('data')
export class DataController {

  constructor(private dataService: DataService) { }


  @Get('/generate/student')
  generateStudent(): void {
    this.dataService.dumpStudent();
  }

  @Get('/generate/teacher')
  generateTeacher(): any {
    return this.dataService.dumpTeacher();
  }
}

import { Controller, Get } from '@nestjs/common';
import { DashboardService } from 'src/provider/dashboard.service';
import { Observable } from 'rxjs';

@Controller('dashboard')
export class DashboardController {

  constructor(
    private dashboardService: DashboardService,
  ) {

  }

  
  @Get('/init')
  findAll(): Observable<any> {
    return this.dashboardService.init();
  }



}

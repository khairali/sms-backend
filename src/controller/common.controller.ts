import { UseInterceptors, UploadedFile, Request, Controller, Post } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path = require('path');
import { v4 as uuidv4 } from 'uuid';

export const storage = {
    storage: diskStorage({
        destination: './uploads/documents',
        filename: (req, file, cb) => {
            const filename: string = path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
            const extension: string = path.parse(file.originalname).ext;

            cb(null, `${filename}${extension}`)
        }
    })
}

export const profileStorage = {
    storage: diskStorage({
        destination: './uploads/profile',
        filename: (req, file, cb) => {
            const filename: string = path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4();
            const extension: string = path.parse(file.originalname).ext;
            cb(null, `${filename}${extension}`)
        }
    })
}

@Controller('common')
export class CommonController {
    constructor() { }

    @Post('/upload')
    @UseInterceptors(FileInterceptor('file', storage))
    uploadFile(@UploadedFile() file, @Request() req): any {
        return file.filename;
    }

    @Post('/upload/profile')
    @UseInterceptors(FileInterceptor('file', profileStorage))
    uploadProfilePic(@UploadedFile() file, @Request() req): any {
        return file.filename;
    }
}

import { Controller, Get, Body, Post } from '@nestjs/common';
import { StudentService } from 'src/provider/student.service';
import { Student } from 'src/schema/student.schema';
import { BaseDTO } from 'src/dto/base.dto';
import { Observable } from 'rxjs';
import { ApplicationParameterService } from 'src/provider/application-parameter.service';
import { ApplicationParameter } from 'src/schema/application-parameter.schema';

@Controller('app-param')
export class ApplicationParameterController {

  constructor(private paramService: ApplicationParameterService) { }


  @Get('/find/all')
  findAll(): Promise<ApplicationParameter[]> {
    return this.paramService.findAll();
  }

}

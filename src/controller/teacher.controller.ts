import { Controller, Get, Post, Body, Param, Headers } from '@nestjs/common';
import { TeacherService } from '../provider/teacher.service';
import { Teacher } from '../schema/teacher.schema';
import { Observable } from 'rxjs';
import { BaseDTO } from 'src/dto/base.dto';

@Controller('employee')
export class TeacherController {

  constructor(private teacherService: TeacherService) { }

  @Post('/login')
  login(@Headers() headers): void {
    console.log(headers.authorization);
  }

  @Post('/find/all')
  findAll(@Body() baseDTO: BaseDTO): Observable<Teacher[]> {
    return this.teacherService.findAll(baseDTO);
  }

  @Get('/find/:id')
  findById(@Param('id') id): Observable<Teacher> {
    return this.teacherService.findById(id);
  }

  @Post('/add')
  save(@Body() teacher: Teacher): Promise<Teacher[]> {
    return this.teacherService.save(teacher);
  }

  @Post('/save/period')
  savePeriod(@Body() teacher: Teacher): Promise<Teacher[]> {
    return this.teacherService.savePeriod(teacher);
  }

  @Post('/search')
  search(@Body() base: BaseDTO): Observable<any> {
    return this.teacherService.search(base);
  }

  @Post('/update')
  update(@Body() teacher: Teacher): Promise<Teacher[]> {
    return this.teacherService.update(teacher);
  }

  @Post('/count')
  count(): Promise<number> {
    return this.teacherService.count();
  }

}

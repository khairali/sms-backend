import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { join } from 'path';

import { AppModule } from './app.module';
var express = require('express');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use('/images',express.static(('uploads/profile'))); //Serves resources from public folder

  // cors s
  const options = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204,
    "credentials": true
  }
  app.enableCors(options);

  //  swagger
  const swaggerOption = new DocumentBuilder()
    .setTitle('The City International School')
    .setDescription('School Management API')
    .setVersion('1.0')
    .addTag('sms,school')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerOption);
  SwaggerModule.setup('api', app, document);
  console.log(__dirname);

  await app.listen(process.env.PORT || 3000);
}
bootstrap();

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { BaseService } from './base.service';
import { ApplicationParameter } from 'src/schema/application-parameter.schema';

@Injectable()
export class ApplicationParameterService extends BaseService<ApplicationParameter> {

    constructor(
        @InjectModel(ApplicationParameter.name) private appParamModel: Model<ApplicationParameter>

    ) {
        super();
    }

    findAll(): Promise<ApplicationParameter[]> {
        return this.appParamModel.find().exec();
    }

}
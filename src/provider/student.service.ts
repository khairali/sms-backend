import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { DocumentQuery, Model } from 'mongoose';
import { FeeHistory, Student } from '../schema/student.schema';
import { BaseDTO } from 'src/dto/base.dto';
import { from, Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { QueryDTO } from 'src/dto/query.dto';
import { PaginationDTO } from 'src/dto/pagination.dto';
import { invoice, org_config } from 'src/app.config';
import { generateTemplate } from 'src/util/voucher-util';
import { StudentDTO } from 'src/dto/student.dto';
import { PENDING, randomAlphaNumeric } from 'src/util/app-util';
import { response, Response } from 'express';
import { TableResponse } from 'src/dto/table-response.dto';
import { ObjectId } from 'mongodb';
import { app_url } from '../app.config';
import { ApplicationParameterService } from './application-parameter.service';
import { addDays, getDate, getDay, getMonth, getYear } from 'date-fns';
import { ApplicationParameter } from 'src/schema/application-parameter.schema';

@Injectable()
export class StudentService extends BaseService<Student> {

    constructor(
        @InjectModel(Student.name) private studentModel: Model<Student>,
        private parameterService: ApplicationParameterService,
        private httpService: HttpService
    ) {
        super();
    }

    async findAll(baseDTO: BaseDTO): Promise<TableResponse<Student[]> | Student[]> {
        const pagination = baseDTO.pagination;
        if (pagination.limit > 0) {
            const data$ = await this.studentModel.find({}, baseDTO.projection)
                .skip(pagination.offset)
                .limit(pagination.limit)
                .exec();
            const count$ = await this.count();
            return new Promise(resolve => {
                resolve({
                    count: count$,
                    data: data$
                } as TableResponse<Student[]>)
            })
        } else {
            const data$ = await this.studentModel.find({}).exec();
            return new Promise(resolve => resolve(data$))
        }
        // forkJoin(data$, this.count()).pipe(map(data => this.toBaseDTO(data)));
    }

    async findById(id: string): Promise<Student> {
        return await this.studentModel.findById({ _id: id }).exec();
    }

    async save(student: Student): Promise<any> {
        // console.log('save student', student);
        let rollNo = await this.assignRollNo(student.grade);
        let params = await this.parameterService.findAll() as ApplicationParameter[];
        let admissionFee = params[0].admissionFee;
        let dueDay = params[0].dueDay;
        let fee = params[0].fee[student.grade];
        let date = new Date();
        let voucher = {
            day: getDate(date),
            month: getMonth(date) + 1,
            year: getYear(date),
            generatedAt: new Date(),
            voucherNo: randomAlphaNumeric(6),
            status: PENDING,
            fee: (fee + admissionFee),
            dueDate: addDays(date, dueDay)
        } as FeeHistory;
        console.log(fee, admissionFee);
        student.rollNo = rollNo;
        student.feeHistory = [voucher]
        const studentModel = new this.studentModel(student);
        return studentModel.save();
        // return null
    }

    async assignRollNo(grade: string): Promise<number> {
        let result = await this.studentModel.aggregate([
            { $match: { grade: grade } },
            {
                $group: {
                    _id: null,
                    maxQuantity: { $max: "$rollNo" }
                }
            }
        ]).exec();
        let rollNo = 1;
        if (result.length > 0) {
            rollNo = result[0].maxQuantity;
        }
        return rollNo;
    }

    update(student: Student): Promise<any> {
        console.log('update student', student);
        const studentModel = new this.studentModel(student);
        return studentModel.updateOne(student).exec();
    }

    updateNewFeeHistory(_id: string, fee: FeeHistory): Promise<any> {
        return this.studentModel.updateOne(
            { _id: new ObjectId(_id) },
            {
                $push: {
                    "feeHistory": fee
                }
            }
        ).exec()
    }

    updateVoucher(student: StudentDTO): Promise<any> {
        return this.studentModel.updateOne(
            { _id: new ObjectId(student._id), "feeHistory.voucherNo": student.voucherInfo.voucherNo },
            { $set: { "feeHistory.$.voucherImage": student.voucherInfo.voucherImage, "feeHistory.$.status": student.voucherInfo.status } }
        ).exec();
    }


    searchExtra(base: BaseDTO): Promise<Student[]> {
        console.log('search student', base);
        const studentModel = new this.studentModel();
        let query = base.query as QueryDTO;
        let pagination = base.pagination as PaginationDTO;
        return studentModel.collection
            .find(this.buildQueryRegex(query))
            .skip(pagination.offset)
            .limit(pagination.limit)
            .toArray();
    }

    async search(base: BaseDTO): Promise<TableResponse<Student[]>> {
        console.log('search student', base);
        const studentModel = new this.studentModel();
        let query = base.query as QueryDTO;
        let pagination = base.pagination as PaginationDTO;
        let source$ = studentModel.collection.find({ $text: { $search: query.value } });
        let data$ = await source$
            .skip(pagination.offset)
            .limit(pagination.limit)
            .toArray();
        let count$ = await source$.count();
        let response = {
            count: count$,
            data: data$,
        } as TableResponse<Student[]>;
        return response
    }


    async count(): Promise<number> {
        const studentModel = new this.studentModel();
        return await studentModel.collection.count();
    }

    aggregateByGender(): Promise<any> {
        const studentModel = new this.studentModel();
        return studentModel.collection.aggregate([
            {
                $group: {
                    _id: "$gender",
                    count: { $sum: 1 }
                }
            }
        ]).toArray()
    }

    async generateVoucher(studentDTO: StudentDTO, res: Response): Promise<Buffer> {
        let student = await this.findById(studentDTO._id);
        let history = student.feeHistory.find(d => studentDTO.voucherInfo.voucherNo == d.voucherNo);
        let user = {
            name: `${student.firstName} ${student.lastName}`,
            rollNo: student.rollNo,
            grade: student.grade,
            voucherInfo: history,
            fatherName: student.fatherName,
        } as StudentDTO;
        const PDFDocument = require("pdfkit");
        const pdfBuffer: Buffer = await new Promise(resolve => {
            const doc = new PDFDocument({
                size: 'LETTER',
                bufferPages: true,
            })
            generateTemplate(doc, user);
            doc.end()
            const buffer = []
            doc.on('data', buffer.push.bind(buffer))
            doc.on('end', () => {
                const data = Buffer.concat(buffer)
                resolve(data)
            })
        })

        let fileName = user.name + "_" + (user.voucherInfo.generatedAt.getMonth() + 1) + "_" + user.voucherInfo.generatedAt.getFullYear()
        res.set({
            'Content-Type': 'application/pdf',
            'Content-Disposition': `${fileName}.pdf`,
            'Content-Length': pdfBuffer.length,
        });
        return pdfBuffer
    }

    async downlaodVoucher(studentDTO: StudentDTO, res: Response): Promise<Buffer> {
        let student = await this.findById(studentDTO._id);
        let history = student.feeHistory.find(d => studentDTO.voucherInfo.voucherNo == d.voucherNo);
        const PDFDocument = require("pdfkit");
        const pdfBuffer: Buffer = await new Promise(resolve => {
            const doc = new PDFDocument({
                size: 'LETTER',
                bufferPages: true,
            })
            doc
                .font('Times-Roman')
                .image(`./uploads/documents/${history.voucherImage}`, 30, 30, { scale: 0.8, align: 'center', valign: 'center' })
            doc.end()
            const buffer = []
            doc.on('data', buffer.push.bind(buffer))
            doc.on('end', () => {
                const data = Buffer.concat(buffer)
                resolve(data)
            })
        })

        let fileName = student.firstName + "_" + student.lastName + "_" + history.voucherNo + "_" + history.month + "_" + history.year;
        res.set({
            'Content-Type': 'application/pdf',
            'Content-Disposition': `${fileName}.pdf`,
            'Content-Length': pdfBuffer.length,
        });

        return pdfBuffer;

    }




}
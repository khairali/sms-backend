import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Teacher } from 'src/schema/teacher.schema';
import { Model } from 'mongoose';
import { BaseDTO } from 'src/dto/base.dto';
import { Observable, from, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { ObjectId } from 'mongodb';
import { QueryDTO } from 'src/dto/query.dto';
import { PaginationDTO } from 'src/dto/pagination.dto';

@Injectable()
export class TeacherService extends BaseService<Teacher> {

    constructor(@InjectModel(Teacher.name) private teacherModel: Model<Teacher>) {
        super();
    }

    findAll(baseDTO: BaseDTO): Observable<any> {
        const pagination = baseDTO.pagination;
        const data$ = from(this.teacherModel.find({}, baseDTO.projection)
            .skip(pagination.offset)
            .limit(pagination.limit)
            .exec());
        return forkJoin(data$, this.count()).pipe(map(data => this.toBaseDTO(data)));
    }

    findById(id: string): Observable<Teacher> {
        return from(this.teacherModel.findById({ _id: id }).exec());
    }

    save(teacher: Teacher): Promise<any> {
        console.log('teacher', teacher);
        teacher.type = 'teacher';
        const teacherModel = new this.teacherModel(teacher);
        return teacherModel.save();
    }

    savePeriod(teacher: Teacher): Promise<any> {
        return this.teacherModel.updateOne({ _id: new ObjectId(teacher._id) }, { $set: { periods: teacher.periods } }).exec();
    }

    search(base: BaseDTO): Observable<any> {
        console.log('search teacher', base);
        const teacherModel = new this.teacherModel();
        let query = base.query as QueryDTO;
        let pagination = base.pagination as PaginationDTO;
        let source$ = teacherModel.collection.find({ $text: { $search: query.value } });
        let data$ = from(source$
            .skip(pagination.offset)
            .limit(pagination.limit)
            .toArray());
        let count$: Observable<any> = from(source$.count());

        return forkJoin(data$, count$).pipe(map(data => this.toBaseDTO(data)));

    }

    update(teacher: Teacher): Promise<any> {
        console.log('update student', teacher);
        const teacherModel = new this.teacherModel(teacher);
        return teacherModel.updateOne(teacher).exec();

    }

    count(): Promise<any> {
        const studentModel = new this.teacherModel();
        return this.teacherModel.collection.count();
    }

    aggregateByGender(): Promise<any> {
        const teacherModel = new this.teacherModel();
        return teacherModel.collection.aggregate([
            {
                $group: {
                    _id: "$gender",
                    count: { $sum: 1 }
                }
            }
        ]).toArray();
    }


}

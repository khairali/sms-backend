import { HttpService, Injectable } from '@nestjs/common';
import { StudentService } from './student.service';
import { Student } from 'src/schema/student.schema';
import { TeacherService } from './teacher.service';
import { student_dummy } from 'src/data.dummy';
import { config } from 'src/app.config';
import { Teacher } from 'src/schema/teacher.schema';

@Injectable()
export class DataService {

    constructor(
        private studentService: StudentService,
        private teacherService: TeacherService,
        private http: HttpService
    ) { }

    dumpStudent(): any {
        this.http.get(config.dev.mock_student_url)
            .subscribe(response => {
                for (let i = 0; i < response.data.length; i++) {
                    this.studentService.save(response.data[i] as Student);
                }
            })
    }

    dumpTeacher(): any {
        this.http.get(config.dev.mock_teacher_url)
            .subscribe(response => {
                for (let i = 0; i < response.data.length; i++) {
                    this.teacherService.save(response.data[i] as Teacher);
                }
            })
    }

    cleanAllStudent(): void {
        // this.studentService.delete({});
    }



}

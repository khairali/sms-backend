
export function rand(length, type: 'c' | 'g' | 'n'| 'cnic') {
    if(type === 'cnic'){
        return `${rand(5,'n' )}-${rand(7,'n' )}-${rand(1,'n' )}`
    }
    var source = {
        c: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        g: 'mf',
        n: '123456789',
    }
    var result = '';
    var charactersLength = source[type].length;
    for (var i = 0; i < length; i++) {
        result += source[type].charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


import { Injectable } from '@nestjs/common';
import { StudentService } from './student.service';
import { TeacherService } from './teacher.service';
import { Observable, from, concat, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DashboardService {

    constructor(
        private studentService: StudentService,
        private teacherService: TeacherService
    ) {
    }


    init(): Observable<any> {
        let student$ = from(this.studentService.aggregateByGender()).pipe(map(data => data = this.toObj('student', data)));
        let teacher$ = from(this.teacherService.aggregateByGender()).pipe(map(data => data = this.toObj('teacher', data)));
        return forkJoin(student$, teacher$);
    }


    toObj(type, data): any {
        return { type, data }
    }


}

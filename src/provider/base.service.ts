import { Injectable } from '@nestjs/common';
import { BaseDTO } from 'src/dto/base.dto';
import { BaseSchema } from 'src/schema/base.schema';
import { QueryDTO } from 'src/dto/query.dto';

@Injectable()
export class BaseService<T extends BaseSchema> {

    constructor() { }

    toBaseDTO(data: T[]): BaseDTO {
        const base = {} as BaseDTO;
        base.data = data[0];
        base.count = data[1];
        return base
    }

    buildQueryRegex(query: QueryDTO): any {
        let bucket = [];
        for (let k in query.map) {
            bucket.push({
                [k]: { '$regex': `^${query.map[k]}`, "$options": "i" }
            });
        }
        return {
            $and: bucket
        };
    }
}

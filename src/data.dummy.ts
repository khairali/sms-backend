import { Student } from "./schema/student.schema";

export const student_dummy =
    [
        {
            "rollNo": 0,
            "firstName": "Shelia",
            "lastName": "Boyle",
            "dob": "2009-03-27",
            "gender": "m",
            "bFormNo": 296644620760,
            "fatherName": "Dejesus Wiley",
            "fatherCNIC": 630220988647,
            "fatherContact": "brown",
            "motherName": "Angelica Alston",
            "motherCNIC": 719311260408,
            "grade": 4,
            "address": "775 Quay Street, Shasta, Georgia, 8599"
        },
        {
            "rollNo": 1,
            "firstName": "Elma",
            "lastName": "Jones",
            "dob": "2002-02-07",
            "gender": "f",
            "bFormNo": 355178337078,
            "fatherName": "Winnie Roy",
            "fatherCNIC": 697127326433,
            "fatherContact": "brown",
            "motherName": "Foreman Shepard",
            "motherCNIC": 919945546842,
            "grade": 9,
            "address": "262 Llama Court, Spokane, Nebraska, 1685"
        },
        {
            "rollNo": 2,
            "firstName": "Lang",
            "lastName": "Benjamin",
            "dob": "2018-08-31",
            "gender": "f",
            "bFormNo": 790082422736,
            "fatherName": "Felicia Sullivan",
            "fatherCNIC": 367389705822,
            "fatherContact": "blue",
            "motherName": "Brooks Burgess",
            "motherCNIC": 985592864960,
            "grade": 9,
            "address": "573 Bokee Court, Beason, Northern Mariana Islands, 2989"
        },
        {
            "rollNo": 3,
            "firstName": "Marlene",
            "lastName": "Roman",
            "dob": "2018-12-01",
            "gender": "f",
            "bFormNo": 469431345109,
            "fatherName": "Neva Townsend",
            "fatherCNIC": 594791593441,
            "fatherContact": "green",
            "motherName": "Celia Neal",
            "motherCNIC": 265117512241,
            "grade": 6,
            "address": "989 Beayer Place, Waiohinu, Maine, 1036"
        },
        {
            "rollNo": 4,
            "firstName": "Suarez",
            "lastName": "Walters",
            "dob": "2006-11-21",
            "gender": "m",
            "bFormNo": 901430967208,
            "fatherName": "Deanna Combs",
            "fatherCNIC": 313268707795,
            "fatherContact": "green",
            "motherName": "Rice Puckett",
            "motherCNIC": 467132055910,
            "grade": 5,
            "address": "357 Varet Street, Dorneyville, Oregon, 7123"
        },
        {
            "rollNo": 5,
            "firstName": "Davis",
            "lastName": "Jacobs",
            "dob": "2016-05-20",
            "gender": "f",
            "bFormNo": 455430758639,
            "fatherName": "Phillips Cochran",
            "fatherCNIC": 426269269640,
            "fatherContact": "brown",
            "motherName": "Allison Gordon",
            "motherCNIC": 972370013126,
            "grade": 9,
            "address": "663 Arion Place, Martinsville, Alaska, 9063"
        },
        {
            "rollNo": 6,
            "firstName": "Geraldine",
            "lastName": "Swanson",
            "dob": "2001-11-13",
            "gender": "m",
            "bFormNo": 322916044702,
            "fatherName": "Velma Gilliam",
            "fatherCNIC": 722938004802,
            "fatherContact": "green",
            "motherName": "Martina Wilcox",
            "motherCNIC": 919623669204,
            "grade": 9,
            "address": "422 Winthrop Street, Walton, District Of Columbia, 1179"
        },
        {
            "rollNo": 7,
            "firstName": "Walters",
            "lastName": "Mercado",
            "dob": "2007-12-28",
            "gender": "f",
            "bFormNo": 311369698383,
            "fatherName": "Luz Burris",
            "fatherCNIC": 791670163184,
            "fatherContact": "green",
            "motherName": "Deleon Duran",
            "motherCNIC": 14674765147,
            "grade": 5,
            "address": "156 Opal Court, Townsend, Missouri, 4175"
        },
        {
            "rollNo": 8,
            "firstName": "Buchanan",
            "lastName": "Rivera",
            "dob": "2014-03-22",
            "gender": "m",
            "bFormNo": 233155715136,
            "fatherName": "Lena Head",
            "fatherCNIC": 582899623621,
            "fatherContact": "green",
            "motherName": "Reba Sharpe",
            "motherCNIC": 982248258949,
            "grade": 2,
            "address": "363 Cambridge Place, Needmore, Michigan, 7874"
        },
        {
            "rollNo": 9,
            "firstName": "Freida",
            "lastName": "Gilmore",
            "dob": "2010-05-15",
            "gender": "m",
            "bFormNo": 28696253629,
            "fatherName": "Lucile Trujillo",
            "fatherCNIC": 403649040646,
            "fatherContact": "blue",
            "motherName": "Dillon Ford",
            "motherCNIC": 503072142876,
            "grade": 2,
            "address": "855 Mill Street, Bordelonville, Massachusetts, 5969"
        },
        {
            "rollNo": 10,
            "firstName": "Michael",
            "lastName": "Rojas",
            "dob": "2004-04-19",
            "gender": "m",
            "bFormNo": 350327561677,
            "fatherName": "Dillard Oneill",
            "fatherCNIC": 556655532500,
            "fatherContact": "green",
            "motherName": "Jaime Downs",
            "motherCNIC": 659284124080,
            "grade": 3,
            "address": "888 Hampton Avenue, Clinton, Florida, 7284"
        },
        {
            "rollNo": 11,
            "firstName": "Marian",
            "lastName": "Long",
            "dob": "2011-05-01",
            "gender": "m",
            "bFormNo": 415936493417,
            "fatherName": "Tonya Stephenson",
            "fatherCNIC": 537035822264,
            "fatherContact": "brown",
            "motherName": "Holloway Pittman",
            "motherCNIC": 569389858623,
            "grade": 2,
            "address": "112 Voorhies Avenue, Crayne, Illinois, 785"
        },
        {
            "rollNo": 12,
            "firstName": "Alford",
            "lastName": "Richards",
            "dob": "2001-07-01",
            "gender": "m",
            "bFormNo": 702080807155,
            "fatherName": "Erica Manning",
            "fatherCNIC": 174563419855,
            "fatherContact": "blue",
            "motherName": "Pamela Sargent",
            "motherCNIC": 939157285670,
            "grade": 1,
            "address": "631 Dinsmore Place, Ilchester, Wyoming, 8802"
        },
        {
            "rollNo": 13,
            "firstName": "Levine",
            "lastName": "Huber",
            "dob": "2019-10-09",
            "gender": "m",
            "bFormNo": 461359482102,
            "fatherName": "Moody Wood",
            "fatherCNIC": 334862486173,
            "fatherContact": "blue",
            "motherName": "Patton Moreno",
            "motherCNIC": 93758507088,
            "grade": 5,
            "address": "804 Cypress Avenue, Ypsilanti, Idaho, 8266"
        },
        {
            "rollNo": 14,
            "firstName": "Gordon",
            "lastName": "Boyd",
            "dob": "2005-08-07",
            "gender": "m",
            "bFormNo": 34277583264,
            "fatherName": "Jarvis Patterson",
            "fatherCNIC": 60660424695,
            "fatherContact": "blue",
            "motherName": "Kristy Hurley",
            "motherCNIC": 679919254255,
            "grade": 1,
            "address": "723 Sullivan Place, Ernstville, California, 9344"
        },
        {
            "rollNo": 15,
            "firstName": "Price",
            "lastName": "Hampton",
            "dob": "2005-12-13",
            "gender": "m",
            "bFormNo": 112880169591,
            "fatherName": "Nancy Hawkins",
            "fatherCNIC": 114996414649,
            "fatherContact": "blue",
            "motherName": "Garrett Pratt",
            "motherCNIC": 795560130379,
            "grade": 7,
            "address": "896 Montgomery Place, Waukeenah, Utah, 3320"
        },
        {
            "rollNo": 16,
            "firstName": "Jeannie",
            "lastName": "Rivas",
            "dob": "2017-01-07",
            "gender": "m",
            "bFormNo": 215749004396,
            "fatherName": "Joseph Steele",
            "fatherCNIC": 191926258285,
            "fatherContact": "blue",
            "motherName": "Cherry Dawson",
            "motherCNIC": 60061521307,
            "grade": 7,
            "address": "544 Joralemon Street, Katonah, Virginia, 9344"
        },
        {
            "rollNo": 17,
            "firstName": "Wendy",
            "lastName": "Logan",
            "dob": "2000-05-30",
            "gender": "f",
            "bFormNo": 391690310427,
            "fatherName": "Cindy Young",
            "fatherCNIC": 92015491074,
            "fatherContact": "blue",
            "motherName": "Georgina Hubbard",
            "motherCNIC": 337148750725,
            "grade": 4,
            "address": "165 Dekoven Court, Mapletown, New Hampshire, 3898"
        },
        {
            "rollNo": 18,
            "firstName": "Melba",
            "lastName": "Justice",
            "dob": "2019-10-16",
            "gender": "m",
            "bFormNo": 791146737347,
            "fatherName": "Eddie Waters",
            "fatherCNIC": 729420943275,
            "fatherContact": "green",
            "motherName": "Silva Kent",
            "motherCNIC": 322591340263,
            "grade": 9,
            "address": "534 Troutman Street, Summertown, Marshall Islands, 6100"
        },
        {
            "rollNo": 19,
            "firstName": "West",
            "lastName": "Small",
            "dob": "2003-01-03",
            "gender": "m",
            "bFormNo": 896859894904,
            "fatherName": "Alta Schwartz",
            "fatherCNIC": 692852776111,
            "fatherContact": "brown",
            "motherName": "Trina Wyatt",
            "motherCNIC": 822965846852,
            "grade": 7,
            "address": "792 Alton Place, Norris, American Samoa, 517"
        },
        {
            "rollNo": 20,
            "firstName": "Judy",
            "lastName": "Mcmahon",
            "dob": "2017-07-28",
            "gender": "m",
            "bFormNo": 724015152773,
            "fatherName": "Wiley Meadows",
            "fatherCNIC": 223133589631,
            "fatherContact": "brown",
            "motherName": "Romero Santiago",
            "motherCNIC": 553819805315,
            "grade": 2,
            "address": "587 Aviation Road, Brookfield, Indiana, 2420"
        },
        {
            "rollNo": 21,
            "firstName": "Shelly",
            "lastName": "Padilla",
            "dob": "2001-02-15",
            "gender": "f",
            "bFormNo": 7014483195,
            "fatherName": "Bertie Savage",
            "fatherCNIC": 801549947206,
            "fatherContact": "brown",
            "motherName": "Mueller Hansen",
            "motherCNIC": 220855950031,
            "grade": 6,
            "address": "633 Amboy Street, Haena, West Virginia, 8985"
        },
        {
            "rollNo": 22,
            "firstName": "Minnie",
            "lastName": "Ellison",
            "dob": "2009-07-18",
            "gender": "m",
            "bFormNo": 15159784328,
            "fatherName": "Juanita Stevens",
            "fatherCNIC": 417840509411,
            "fatherContact": "blue",
            "motherName": "Sweet Richmond",
            "motherCNIC": 679672230327,
            "grade": 2,
            "address": "281 Dupont Street, Belleview, Arizona, 8426"
        },
        {
            "rollNo": 23,
            "firstName": "Britney",
            "lastName": "Rasmussen",
            "dob": "2006-04-20",
            "gender": "f",
            "bFormNo": 571480648973,
            "fatherName": "Bridges Dodson",
            "fatherCNIC": 62655443094,
            "fatherContact": "green",
            "motherName": "Farmer Oneal",
            "motherCNIC": 139927350519,
            "grade": 1,
            "address": "169 Seba Avenue, Murillo, Iowa, 8054"
        },
        {
            "rollNo": 24,
            "firstName": "John",
            "lastName": "Dickson",
            "dob": "2009-12-26",
            "gender": "f",
            "bFormNo": 116441029131,
            "fatherName": "Gloria Ferguson",
            "fatherCNIC": 51180375571,
            "fatherContact": "brown",
            "motherName": "Briana Benton",
            "motherCNIC": 561915006533,
            "grade": 5,
            "address": "838 Elizabeth Place, Twilight, Mississippi, 6016"
        }
    ];


import { PaginationDTO } from './pagination.dto';
import { ApiProperty } from '@nestjs/swagger';
import { QueryDTO } from './query.dto';

export class BaseDTO {

    _id: any;

    @ApiProperty()
    pagination: PaginationDTO;

    @ApiProperty()
    query: QueryDTO;

    @ApiProperty()
    public projection: {};

    data: any;

    count: any;
}
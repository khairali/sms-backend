import { BaseDTO } from "./base.dto";

export class StudentDTO extends BaseDTO {

    name: string;
    fatherName: string;
    rollNo: number;
    bFormNo: string;
    grade: string;
    voucherInfo: {
        generatedAt: Date;
        dueDate: Date;
        year: number;
        month: number;
        day: number;
        fee: number;
        status: string;
        voucherNo: string;
        voucherImage: string;
        paidAt: Date;
    }

}
import { ApiProperty } from "@nestjs/swagger"

export class PaginationDTO{
    @ApiProperty()
    limit:number;
    @ApiProperty()
    offset:number;
}
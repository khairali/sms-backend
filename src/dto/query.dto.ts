import { ApiProperty } from "@nestjs/swagger";

export class QueryDTO {
    @ApiProperty()
    public value: string;
    @ApiProperty()
    public field: string

    @ApiProperty()
    public map: {}

}
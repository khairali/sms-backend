export interface TableResponse<T> {
    count: number;
    data: T,
}

import { Injectable, Logger } from "@nestjs/common";
import { Cron } from "@nestjs/schedule";
import { BaseDTO } from "src/dto/base.dto";
import { StudentService } from "src/provider/student.service";
import { FeeHistory, Student } from "src/schema/student.schema";
import { randomAlphaNumeric } from "src/util/app-util";
import { format, add, addDays } from 'date-fns'
import { ApplicationParameter } from "src/schema/application-parameter.schema";
import { ApplicationParameterService } from "src/provider/application-parameter.service";

@Injectable()
export class VoucherService {
    private readonly logger = new Logger(VoucherService.name);

    constructor(
        private studentService: StudentService,
        private paramService: ApplicationParameterService
    ) { }

    @Cron('*/50 * * * * *')
    async generateVoucher() {
        const dueDays = 13;
        let base = {
            pagination: {
                limit: -1,
                offset: 0
            }
        } as BaseDTO;
        let param = await this.paramService.findAll();
        let students = await this.studentService.findAll(base) as Student[];
        let month = new Date().getMonth() + 1;
        let day = new Date().getDate();
        let year = new Date().getFullYear();
        for (let s of students) {
            let feeHistory = s.feeHistory.find(d => d.day == day && d.month == month);
            if (!feeHistory) {
                feeHistory = {
                    day: day,
                    month: month,
                    year: year,
                    status: 'p',
                    fee: 2100,
                    voucherNo: randomAlphaNumeric(6),
                    generatedAt: new Date(),
                    dueDate: addDays(new Date(), param[0].dueDay),
                    voucherImage: null
                } as FeeHistory;
                let result = await this.studentService.updateNewFeeHistory(s._id, feeHistory);
                console.log(result);
            }
        }
    }
}
export const app_url = `http://localhost:3000`
export const config = {
    dev: {
        mock_student_url : 'https://next.json-generator.com/api/json/get/EJPBzXZ_K',
        mock_teacher_url : 'https://next.json-generator.com/api/json/get/Ekl7VolKK',
    } 
};


export const org_config = {
    name:'ABC School',
    address:'Street 42 near shopping mal',
    city:'Rawalpindi',
    logoURL:'assets/images/school-logo.png'
}

export const  invoice = {
    shipping: {
      name: "Khair Ali",
      address: "1234 Main Street",
      city: "Rawalpindi",
      state: "Punjab",
      country: "Pakistan",
      postal_code: 46000
    },
    items: [
      {
        item: "USB_EXT",
        description: "USB Cable Extender",
        quantity: 1,
        amount: 2000
      }
    ],
    subtotal: 8000,
    paid: 0,
    invoice_nr: 1234
  };
  
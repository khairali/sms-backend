import { format, add } from 'date-fns';


export function addDays(date: Date, days) {
    return add(date, {
        years: 0,
        months: 0,
        weeks: 0,
        days: days,
        hours: 0,
        minutes: 0,
        seconds: 0,
    })
}
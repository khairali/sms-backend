import { org_config } from "src/app.config";
import { StudentDTO } from "src/dto/student.dto";

export function generateTemplate(doc, user: StudentDTO) {
  let moveToX = (count) => x = x + count;
  let moveToY = (count) => y = y + count;
  let { x, y } = generateStaffCopy(doc, 50, 45, user);
  moveToX(40);
  moveToY(400)
  doc.moveTo(0, x).lineTo(y, x).stroke();
  generateStudentCopy(doc, 50, x + 20, user);
}

function generateStaffCopy(doc, y, x, user: StudentDTO): any {
  let moveToX = (count) => x = x + count;
  let moveToY = (count) => y = y + count;
  // header
  doc
    .font('Times-Roman')
    .image(`${org_config.logoURL}`, moveToY(0), moveToX(0), { width: 50 })
    .fillColor("#444444")
    .fontSize(20)
    .text(`${org_config.name}`, moveToY(60), moveToX(12))
    .fontSize(10)
    .text(`${org_config.address}`, moveToY(90), moveToX(8), { align: "right" })
    .text(`${org_config.city}`, moveToY(0), moveToX(15), { align: "right" })
    .moveDown();

  moveToX(50);
  y = 50;
  doc
    .font('Times-Roman')
    .fontSize(16)
    .fillColor('black')
    .text(`Student Name: ${user.name}`, y, moveToX(0))
    .text(`Father  Name: ${user.fatherName}`, y, moveToX(20))
    .text(`Grade : ${user.grade}`, y, moveToX(20))
    .text(`Roll No : ${user.rollNo}`, y, moveToX(20))
    .fillColor("red")
    .text(`Fee : ${user.voucherInfo.fee}`, y, moveToX(20))
    .fillColor('black')
    .text(`Voucher Number :${user.voucherInfo.voucherNo}`, y, moveToX(30))
    .text(`Generated At : ${user.voucherInfo.generatedAt.getDay()}-${user.voucherInfo.generatedAt.getMonth() + 1}-${user.voucherInfo.generatedAt.getFullYear()}`, y, moveToX(20))
    .text(`Due Date : ${user.voucherInfo.dueDate.getDay()}-${user.voucherInfo.dueDate.getMonth() + 1}-${user.voucherInfo.dueDate.getFullYear()}`, y, moveToX(20))
    .moveDown();

  doc
    .fontSize(12)
    .fillColor('black')
    .text(`Parent Signature`, y, moveToX(60))
    .text(`Staff Signature`, moveToY(250), moveToX(0))
    .fillColor('red')
    .font('Times-Italic')
    .text(`Staff Copy`, { align: "right", continued: false })
    .moveDown();
  return { x, y };
}

function generateStudentCopy(doc, y, x, user: StudentDTO) {
  let currentDate = new Date();
  let moveToX = (count) => x = x + count;
  let moveToY = (count) => y = y + count;

  doc
    .font('Times-Roman')
    .image(`${org_config.logoURL}`, moveToY(0), moveToX(0), { width: 50 })
    .fillColor("#444444")
    .fontSize(20)
    .text(`${org_config.name}`, moveToY(60), moveToX(12))
    .fontSize(10)
    .text(`${org_config.address}`, moveToY(90), moveToX(8), { align: "right" })
    .text(`${org_config.city}`, moveToY(0), moveToX(15), { align: "right" })
    .moveDown();

  moveToX(50);
  y = 50;

  doc
    .font('Times-Roman')
    .fontSize(16)
    .fillColor('black')
    .text(`Student Name: ${user.name}`, y, moveToX(0))
    .text(`Father  Name: ${user.fatherName}`, y, moveToX(20))
    .text(`Grade : ${user.grade}`, y, moveToX(20))
    .text(`Roll No : ${user.rollNo}`, y, moveToX(20))
    .fillColor("red")
    .text(`Fee : ${user.voucherInfo.fee}`, y, moveToX(20))
    .fillColor('black')
    .text(`Voucher Number :${user.voucherInfo.voucherNo}`, y, moveToX(30))
    .text(`Generated At : ${user.voucherInfo.generatedAt.getDay()}-${user.voucherInfo.generatedAt.getMonth() + 1}-${user.voucherInfo.generatedAt.getFullYear()}`, y, moveToX(20))
    .text(`Due Date : ${user.voucherInfo.dueDate.getDay()}-${user.voucherInfo.dueDate.getMonth() + 1}-${user.voucherInfo.dueDate.getFullYear()}`, y, moveToX(20))
    .moveDown();

  doc
    .fontSize(12)
    .text(`Parent Signature`, y, moveToX(60))
    .text(`Staff Signature`, moveToY(250), moveToX(0))
    .fillColor('red')
    .font('Times-Italic')
    .text(`Student Copy`, { align: "right" })
    .moveDown();
  return x;
}

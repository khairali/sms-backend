import { HttpModule, Module } from '@nestjs/common';
import { AppService } from './app.service';
import { StudentService } from './provider/student.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Student, StudentSchema } from './schema/student.schema';
import { StudentController } from './controller/student.controller';
import { TeacherController } from './controller/teacher.controller';
import { TeacherService } from './provider/teacher.service';
import { Teacher, TeacherSchema } from './schema/teacher.schema';
import { BaseService } from './provider/base.service';
import { DashboardController } from './controller/dashboard.controller';
import { DashboardService } from './provider/dashboard.service';
import { DataController } from './controller/data.controller';
import { DataService } from './provider/data.service';
import { ApplicationParameter, ApplicationParameterSchema } from './schema/application-parameter.schema';
import { ApplicationParameterController } from './controller/application-parameter.controller';
import { ApplicationParameterService } from './provider/application-parameter.service';
import { CommonController } from './controller/common.controller';
import { ScheduleModule } from '@nestjs/schedule';
import { VoucherService } from './scheduler/voucher.scheduler';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://spartan:mongo@db@cluster0.o1qhy.mongodb.net/sms?retryWrites=true&w=majority'),
    MongooseModule.forFeature([{ name: Student.name, schema: StudentSchema, collection: "student_info" }]),
    MongooseModule.forFeature([{ name: Teacher.name, schema: TeacherSchema, collection: "teacher_info" }]),
    MongooseModule.forFeature([{ name: ApplicationParameter.name, schema: ApplicationParameterSchema, collection: "application_parameter" }]),
    HttpModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [StudentController, TeacherController, DashboardController, DataController, ApplicationParameterController, CommonController],
  providers: [AppService, StudentService, TeacherService, BaseService, DashboardService, DataService, ApplicationParameterService, VoucherService],
})
export class AppModule {
}

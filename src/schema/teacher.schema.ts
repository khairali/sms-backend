import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { BaseSchema } from './base.schema';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class Teacher extends BaseSchema {

    @Prop()
    @ApiProperty()
    firstName: string;

    @Prop()
    @ApiProperty()
    lastName: string;

    @Prop()
    @ApiProperty()
    gender: string;

    @Prop()
    @ApiProperty()
    dob: string;

    @Prop()
    @ApiProperty()
    cnic: string;

    @Prop()
    @ApiProperty()
    fatherName: string;

    @Prop()
    @ApiProperty()
    motherName: string;

    @Prop()
    @ApiProperty()
    education: string[];

    @Prop()
    @ApiProperty()
    contact: string;

    @Prop()
    @ApiProperty()
    email: string;

    @Prop()
    @ApiProperty()
    dateOfJoining: string

    @Prop()
    @ApiProperty()
    type: string

    @Prop()
    @ApiProperty()
    address: string;

    @Prop()
    @ApiProperty()
    profilePic: string;

    @Prop()
    @ApiProperty()
    periods: any[];
}

export const TeacherSchema = SchemaFactory.createForClass(Teacher);


import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { BaseSchema } from './base.schema';
import { ApiProperty } from '@nestjs/swagger';


@Schema()
export class Student extends BaseSchema {

    @Prop()
    @ApiProperty()
    firstName: string;
    @Prop()
    @ApiProperty()
    lastName: string;

    @Prop()
    @ApiProperty()
    dob: string;

    @Prop()
    @ApiProperty()
    gender: string;

    @Prop()
    @ApiProperty()
    bFormNo: string;

    @Prop()
    @ApiProperty()
    grade: string;

    @Prop()
    @ApiProperty()
    rollNo: number;

    @Prop()
    @ApiProperty()
    fatherName: string;

    @Prop()
    @ApiProperty()
    fatherCNIC: string;

    @Prop()
    @ApiProperty()
    fatherContact: string

    @Prop()
    @ApiProperty()
    fatherEmail: string

    @Prop()
    @ApiProperty()
    motherName: string;

    @Prop()
    @ApiProperty()
    motherCNIC: string;

    @Prop()
    @ApiProperty()
    address: string;

    @Prop()
    @ApiProperty()
    profilePic: string;

    @Prop()
    @ApiProperty()
    feeHistory: FeeHistory[];

}

export interface FeeHistory {
    year: number;
    month: number;
    day: number;
    fee: number;
    status: string;
    voucherNo: string;
    voucherImage: string;
    generatedAt: Date;
    paidAt: Date;
    dueDate: Date;
}

export const StudentSchema = SchemaFactory.createForClass(Student);


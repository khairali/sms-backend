
import { Document } from 'mongoose';
import { Schema, Prop } from '@nestjs/mongoose';


@Schema()
export class BaseSchema extends Document {

    @Prop()
    createDate?: string;

    @Prop()
    modifyDate?: string;

    @Prop()
    createdBy?: string;

    count?: number;

}
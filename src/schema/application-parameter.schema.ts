import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { BaseSchema } from './base.schema';
import { ApiProperty } from '@nestjs/swagger';


@Schema()
export class ApplicationParameter extends BaseSchema {

    @Prop()
    @ApiProperty()
    gender: any[];

    @Prop()
    @ApiProperty()
    education: any[];

    @Prop()
    @ApiProperty()
    month: any;

    @Prop()
    @ApiProperty()
    feeStatus: any;

    @Prop()
    @ApiProperty()
    admissionFee: number;

    @Prop()
    @ApiProperty()
    fee: any;

    @Prop()
    @ApiProperty()
    dueDay: number;
}

export const ApplicationParameterSchema = SchemaFactory.createForClass(ApplicationParameter);

